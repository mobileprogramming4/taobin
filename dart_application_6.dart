import 'dart:io';

var type;
var menu;
void main(List<String> arguments) {
  selectType();
  showMenu();
  if (selectPayment() == true) {
    showPayment();
  } else {
    print("\nCANCELED ORDER");
  }
}

showPayment() {
  checkType();
  print("\nPAID SUCCESSFULLY\n");
  print("~WAITING FOR YOUR ORDER~\n");
  print("~YOUR ORDER $menu SUCCESSFULLY~");
}

void checkType() {
  if (type == "tea") {
    print("\n45 BAHT");
  } else {
    print("\n40 BAHT");
  }
}

void selectType() {
  print("Recommend | Coffee | Tea");
  type = stdin.readLineSync()?.toLowerCase();
}

void showMenu() {
  switch (type) {
    case "recommend":
      showMenuRecommend();
      selectAddition();
      break;
    case "coffee":
      showMenuCoffee();
      selectAddition();
      break;
    case "tea":
      showMenuTea();
      selectAddition();
      break;
  }
}

void showMenuRecommend() {
  print(" ICED AMERICANO | ICED CAPPUCINO | ICED GREEN TEA");
  menu = stdin.readLineSync()?.toUpperCase();
  if (menu == "ICED GREEN TEA") {
    type = "tea";
  } else {
    type = "coffee";
  }
}

void showMenuCoffee() {
  print("HOT AMERICANO | ICED AMERICANO");
  print("HOT CAPPUCINO | ICED CAPPUCINO");
  print("HOT ESPRESSO | ICED ESPRESSO");
  print("HOT MOCHA | ICED MOCHA");
  print("HOT LATTE | ICED LATTE");
  print("HOT MATCHA LATTE | ICED MATCHA LATTE");
  menu = stdin.readLineSync()?.toUpperCase();
}

void showMenuTea() {
  print("HOT BLACK TEA | ICED BLACK TEA");
  print("HOT TAIWANESE TEA | ICED TAIWANESE TEA");
  print("HOT THAI TEA | ICED THAI TEA");
  print("HOT MATCHA TEA | ICED MATCHA TEA");
  menu = stdin.readLineSync()?.toUpperCase();
}

void selectAddition() {
  print("Select sweetness :");
  print("LOW SWEET | ORIGINAL | MORE SWEET");
  var sweet = stdin.readLineSync()?.toUpperCase();
}

selectPayment() {
  print("Select payment :");
  print("CASH");
  print("QR CODE");
  print("USED/CHECK CUPON");
  print("CANCEL ORDER");
  var pay = stdin.readLineSync()?.toUpperCase();
  if (checkPayment(pay) == false) {
    return false;
  }
  return true;
}

checkPayment(pay) {
  if (pay == "CANCEL ORDER") {
    return false;
  }
  return true;
}
